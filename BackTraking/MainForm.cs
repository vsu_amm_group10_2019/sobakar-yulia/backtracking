﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackTraking
{
    public partial class MainForm : Form
    {
        public Spase3D SetOfPoints = new Spase3D();
        public MainForm()
        {
            InitializeComponent();
        }

        public void RedrowList ()
        {
            listViewPoints.Clear();
            foreach(Point point in SetOfPoints.MainSet)
            {
                listViewPoints.Items.Add(point.toString());
            }
        }

        private void addPointToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            FormAddPoint addPoint = new FormAddPoint();
            addPoint.ShowDialog();
            if (addPoint.okClose)
            {
                SetOfPoints.MainSet.Add(addPoint.GetPoint());
                

            }
            else MessageBox.Show("Failed to add point!");
            RedrowList();
        }

        private void randomPointToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Random Coorder = new Random();
            Point point = new Point(Coorder.Next(0, 10), Coorder.Next(0, 10), Coorder.Next(0, 10));
            SetOfPoints.MainSet.Add(point);
            //MessageBox.Show($"Point {point.toString()} added!");
            RedrowList();

        }

        private void redrawDataGridView(SplittedSubsets splittedSubsets)
        {
            dataGridViewSubsets.Rows.Clear();
            if (splittedSubsets.FirstSubset.Count > splittedSubsets.SecondSubset.Count)
            {
                dataGridViewSubsets.RowCount = splittedSubsets.FirstSubset.Count;
            }
            else
            {
                dataGridViewSubsets.RowCount = splittedSubsets.SecondSubset.Count;
            }

            for (int i = 0; i < splittedSubsets.FirstSubset.Count; i++)
            {
                dataGridViewSubsets.Rows[i].Cells[0].Value = splittedSubsets.FirstSubset[i].toString();
            }

            for (int i = 0; i < splittedSubsets.SecondSubset.Count; i++)
            {
                dataGridViewSubsets.Rows[i].Cells[1].Value = splittedSubsets.SecondSubset[i].toString();
            }

        }

        private void splitOnSubsetsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SplittedSubsets splittedSubsets = SetOfPoints.GetSubsets();
            if (splittedSubsets == null)
            {
                MessageBox.Show("Failed to split");
                return;
            }
            redrawDataGridView(splittedSubsets);
            labelCenter.Text = $"Distance Between Centers {Math.Round(splittedSubsets.GetDistanceBetweenCenters(), 3)}";

        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listViewPoints.Clear();
            dataGridViewSubsets.Rows.Clear();
            SetOfPoints.MainSet.Clear();
            labelCenter.Text = "Distance Between Centers";
        }
    }
}
