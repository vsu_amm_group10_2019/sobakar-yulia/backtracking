﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackTraking
{
    public partial class FormAddPoint : Form
    {
         
        public bool okClose = false;
        public FormAddPoint()
        {
            InitializeComponent();
        }

        public Point GetPoint()
        {
            if (textBoxX.Text != "" && textBoxY.Text != "" && textBoxZ.Text != "")
            {
                return new Point(Convert.ToDouble(textBoxX.Text),
                                 Convert.ToDouble(textBoxY.Text),
                                Convert.ToDouble(textBoxZ.Text));

            }
            return null; 
        } 
        private void buttonOK_Click(object sender, EventArgs e)
        { 
         if(textBoxX.Text != "" && textBoxY.Text != "" && textBoxZ.Text != "" )
            {
                okClose = true;
                this.Close();
            } else
            {
                MessageBox.Show("Incorrect Input!");
            }

        }
    }
}
