﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackTraking
{
    public class SplittedSubsets
    {
        public List<Point> FirstSubset { get; set; } = new List<Point>();
        public List<Point> SecondSubset { get; set; } = new List<Point>();

        public SplittedSubsets()
        {

        }

        private Point GetCenterOfGravity(List<Point> Points)
        {
            // цетнтр тяжести для конечного множества G = (point1 + point2 + ...+pointN)/ N
            double x = 0, y = 0, z = 0;
            int CountN = Points.Count;
            foreach (Point item in Points)
            {
                x += item.CoordX;
                y += item.CoordY;
                z += item.CoordZ;
            }
            x = x / CountN;
            y = y / CountN;
            z = z / CountN;
            Point Center = new Point(x, y, z);
            return Center;
        }
        public double GetDistanceBetweenCenters()
        {
            return GetCenterOfGravity(FirstSubset) - GetCenterOfGravity(SecondSubset);
        }
    }
    public class Spase3D
    {
        public List<Point> MainSet { get; set; } = new List<Point>();

        public SplittedSubsets GetSubsets()
        {
            MainSet = MainSet.Distinct().ToList();
            if (MainSet.Count == 0 || MainSet.Count == 1)
            {
                return null;
                
            }
            double minDist = -1;
            SplittedSubsets bestSplit = new SplittedSubsets();

            for (int i = 0; i < MainSet.Count; i++) 
            {
                for (int countPoint = 0; countPoint < MainSet.Count - 1; countPoint++)
                {
                    SplittedSubsets nowSplit = SplitSet(i, countPoint);
                    double dist = nowSplit.GetDistanceBetweenCenters();

                    if (bestSplit.FirstSubset.Count == 0)
                    {
                        bestSplit = nowSplit;
                        minDist = dist;
                    }
                    else
                    {
                        if (dist < minDist)
                        {
                            bestSplit = nowSplit;
                            minDist = dist;
                        }
                    }

                }               
            }
            return bestSplit; 
        }

        private SplittedSubsets SplitSet(int index, int countPoint)
        {
            int j;
            SplittedSubsets splittedSet = new SplittedSubsets();
            for (j = index; countPoint >= 0; --countPoint)
            {
                splittedSet.FirstSubset.Add(MainSet[j]);
                j++;
                if (j >= MainSet.Count)
                {
                    j = 0;
                }
            }

            while (j != index)
            {
                if (j >= MainSet.Count)
                {
                    j = 0;
                    if (j == index) break;
                }

                splittedSet.SecondSubset.Add(MainSet[j]);
                j++;
            }
            return splittedSet;

        }
    }
}
