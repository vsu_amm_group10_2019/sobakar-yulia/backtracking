﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackTraking
{
    public class Point: IEquatable<Point>
    {
        public double CoordX { get; set; }
        public double CoordY { get; set; }
        public double CoordZ { get; set; }

        public Point()
        {

        }
        public Point(double x, double y, double z)
        {
            CoordX = x;
            CoordY = y;
            CoordZ = z;
        }

        public static double operator -(Point p1, Point p2)
        //расстояние между двумя N-мерным точками
        // d = sqrt( (x1-y1)^2 + (x2-y2)^2 + ... + (xN-yN)^2 )
        {
            return Math.Sqrt( Math.Pow((p1.CoordX - p2.CoordX), 2)
                   + Math.Pow((p1.CoordY - p2.CoordY), 2)
                   + Math.Pow((p1.CoordZ - p2.CoordZ), 2) );
        }


        public string toString()
        {
            return "(" + CoordX + ", " + CoordY + ", " + CoordZ + ")";
        }

        bool IEquatable<Point>.Equals(Point other)
        {
            if (Object.ReferenceEquals(other, null)) return false;

            if (Object.ReferenceEquals(this, other)) return true;

            return CoordX.Equals(other.CoordX) && CoordY.Equals(other.CoordY) && CoordZ.Equals(other.CoordZ);
        }

        public override int GetHashCode()
        {
            int hashProductCoordX = CoordX.GetHashCode();

            int hashProductCoordY = CoordY.GetHashCode();
            int hashProductCoordZ = CoordZ.GetHashCode();

            return hashProductCoordX ^ hashProductCoordY ^ hashProductCoordZ;
        }
    }    

}