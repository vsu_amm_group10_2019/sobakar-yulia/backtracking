﻿
namespace BackTraking
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomPointToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitOnSubsetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listViewPoints = new System.Windows.Forms.ListView();
            this.labelAllPoints = new System.Windows.Forms.Label();
            this.labelSplit = new System.Windows.Forms.Label();
            this.dataGridViewSubsets = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelCenter = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSubsets)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPointToolStripMenuItem,
            this.randomPointToolStripMenuItem,
            this.splitOnSubsetsToolStripMenuItem,
            this.clearToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // addPointToolStripMenuItem
            // 
            this.addPointToolStripMenuItem.Name = "addPointToolStripMenuItem";
            resources.ApplyResources(this.addPointToolStripMenuItem, "addPointToolStripMenuItem");
            this.addPointToolStripMenuItem.Click += new System.EventHandler(this.addPointToolStripMenuItem_Click_1);
            // 
            // randomPointToolStripMenuItem
            // 
            this.randomPointToolStripMenuItem.Name = "randomPointToolStripMenuItem";
            resources.ApplyResources(this.randomPointToolStripMenuItem, "randomPointToolStripMenuItem");
            this.randomPointToolStripMenuItem.Click += new System.EventHandler(this.randomPointToolStripMenuItem_Click_1);
            // 
            // splitOnSubsetsToolStripMenuItem
            // 
            this.splitOnSubsetsToolStripMenuItem.Name = "splitOnSubsetsToolStripMenuItem";
            resources.ApplyResources(this.splitOnSubsetsToolStripMenuItem, "splitOnSubsetsToolStripMenuItem");
            this.splitOnSubsetsToolStripMenuItem.Click += new System.EventHandler(this.splitOnSubsetsToolStripMenuItem_Click);
            // 
            // clearToolStripMenuItem
            // 
            this.clearToolStripMenuItem.Name = "clearToolStripMenuItem";
            resources.ApplyResources(this.clearToolStripMenuItem, "clearToolStripMenuItem");
            this.clearToolStripMenuItem.Click += new System.EventHandler(this.clearToolStripMenuItem_Click);
            // 
            // listViewPoints
            // 
            this.listViewPoints.HideSelection = false;
            resources.ApplyResources(this.listViewPoints, "listViewPoints");
            this.listViewPoints.Name = "listViewPoints";
            this.listViewPoints.UseCompatibleStateImageBehavior = false;
            this.listViewPoints.View = System.Windows.Forms.View.SmallIcon;
            // 
            // labelAllPoints
            // 
            resources.ApplyResources(this.labelAllPoints, "labelAllPoints");
            this.labelAllPoints.Name = "labelAllPoints";
            // 
            // labelSplit
            // 
            resources.ApplyResources(this.labelSplit, "labelSplit");
            this.labelSplit.Name = "labelSplit";
            // 
            // dataGridViewSubsets
            // 
            this.dataGridViewSubsets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSubsets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            resources.ApplyResources(this.dataGridViewSubsets, "dataGridViewSubsets");
            this.dataGridViewSubsets.Name = "dataGridViewSubsets";
            this.dataGridViewSubsets.ReadOnly = true;
            this.dataGridViewSubsets.RowTemplate.Height = 24;
            // 
            // Column1
            // 
            resources.ApplyResources(this.Column1, "Column1");
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            resources.ApplyResources(this.Column2, "Column2");
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // labelCenter
            // 
            resources.ApplyResources(this.labelCenter, "labelCenter");
            this.labelCenter.Name = "labelCenter";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labelCenter);
            this.Controls.Add(this.dataGridViewSubsets);
            this.Controls.Add(this.labelSplit);
            this.Controls.Add(this.labelAllPoints);
            this.Controls.Add(this.listViewPoints);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSubsets)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomPointToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem splitOnSubsetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearToolStripMenuItem;
        private System.Windows.Forms.ListView listViewPoints;
        private System.Windows.Forms.Label labelAllPoints;
        private System.Windows.Forms.Label labelSplit;
        private System.Windows.Forms.DataGridView dataGridViewSubsets;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.Label labelCenter;
    }
}

